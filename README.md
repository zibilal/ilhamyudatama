# Review:

#### Ceritakan apa itu OOP, object oriented programming
**Jawaban**

*OOP adalah paradigma pemrogaman yang berbasis Objek, pembuatan program dengan konsep ini sangatlah memudahkan seorang programmer. Terdapat beberapa konsep yang teridiri dari data abstraksi, konsep yg menyembunyikan detail latar belakang dan hanya mewakili informasi yang diperlukan untuk dunia luar, dan juga enkapsulasi data yang menggabungkan data member dan fungsi, agar dapat mencegah akses data secara langsung. Ada juga konsep polymorphism, inheritance.*

**Nilai**

| | |
| --- | --- |
| Maksimum | 4 |
| Nilai | 1 |
| Review | Jawaban terlalu dangkal. Bagian dasar dari OOP disebutkan dengan tepat, encapsulation dijelaskan terlalu dangkal. Polymorphism dan inheritance hanya disebutkan tanpa ada penjelasannya |

#### Di java 8 ada dua komponen untuk implementasi abstraksi lebih baik, yaitu stream dan lambda. Ceritakan apa itu. Pada saat apakah kita membutuhkan stream atau lambda.

**Jawaban**

*Stream adalah urutan atau elemen yang mendukung operasi sekuensial atau paralel pada itemnya 
Lambda Expression adalah fungsi yang tidak memiliki nama untuk memudahkan suatu method untuk tidak bisa dipanggil lagi seperti fungsi biasa, dan biasanya digunakan pada saat  mengimplementasikan interface dan class abstrak.*

**Nilai**

| | |
| --- | --- |
| Maksimum | 4 |
| Nilai | 0.5 |
| Review | Jawaban terlalu dangkal.| 


#### Apa perbedaan antara interface, abstract kelas.

**Jawaban**

*Class abstract adalah kelas yang tidak memiliki implementasi yang menggunakan extends dan tidak mempunyai bentuk fungsi kongkrit, dengan begitu harus menambahkan fungsi yg lebih jelas. Sedangkan interface yang membentuk komunikasi fungsi dengan fungsi lainnya tetapi hanya berisi blok method yg tidak berubah.*

**Nilai**

| | |
| --- | --- |
| Maksimum | 3 |
| Nilai | 0.5 |
| Review | Jawaban abstract terlalu dangkal. Jawaban untuk interface kurang tepat |


#### Apa itu functional interface, dibutuhkan pada saat apakah?

**Jawaban**

*Functional Interface digunakan untuk memastikan bahwa functional interface tidak boleh memiliki lebih dari satu metode abstrak*

**Nilai**

| | |
| --- | --- |
| Maksimum | 3 |
| Nilai | 0.5 |
| Review | Jawaban terlalu dangkal.Jawaban juga tidak menjawab pertanyaan kedua, "saat dibutuhkan" |

#### Terangkan apa itu SOLID principle. Apakah berguna?

**Jawaban**

*SOLID principle dapat disebut juga sebagai etika dalam pemrogaman, dengen begitu terdapat 5 hal disiplin yang harus dipenuhi. Yang pertama jangan menjadikan suatu kelas menjadi kelas yang serba bisa / core class, buat lah seseimbang mungkin sesuai kebutuhan yang diperlukan. Kedua, jika ingin menggunakan class tanpa harus mengubah class yang sudah dibuat terdahulu sebaiknya mengextends class tersebut tanpa harus mengubahnya. Ketiga, untuk mempercepat alur data lebih baik membuat jalan pintas dengan membuat class peranakan yang singkat tetapi tetap mencakup class parentnya. Keempat, buatlah sebuah interface dengan memiliki fungsi masing – masing yang spesifik. Kelima , jika ingin membuat high level class sebaiknya jangan bergantung kepada low level class.*

**Nilai**

| | |
| --- | --- |
| Maksimum | 4 |
| Nilai | 1.5 |
| Review | Jawaban terlalu dangkal.Jawaban juga tidak menjawab pertanyaan kedua, "apakah berguna" |


#### Jika saya mempunyai integer : 1234567 , buat lah fungsi yang keluarannya 7654321 tanpa menggunakan library, atau string  prosesing.

**Nilai**

| | |
| --- | --- |
| Maksimum | 2 |
| Nilai | 1 |
| Review | Pembuatan public class salah sehingga program tidak dapat dijalankan. Namun jawaban logic-nya sudah benar |


#### Saya memiliki matrix sebagai berikut:

![soal/Screen_Shot_2020-10-05_at_06.48.00.png](./soal/Screen_Shot_2020-10-05_at_06.48.00.png)

Buat matrix class yang mempunya behavior untuk perkalian matrix. Coba untuk menyelesaikan perkalian matrix diatas.

**Nilai**

| | |
| --- | --- |
| Maksimum | 3 |
| Nilai | 0.5 |
| Review | Pembuatan public class salah sehingga program tidak dapat dijalankan, dan jawaban atas kelas tidak tepat |

#### Apa itu unit testing. Bedakah dengan integration testing? Apa bedanya?

**Jawaban**

*Unit Testing adalah pengujian bagian terendah dari aplikasi yang biasanya pengujian function atau object, pengujian yg dilakukan dibilang harus cukup tepat karna mencakup seluruh kemungkinan penggunaan objek yang digunakan. Setelah itu ada integration testing yaitu pengujian keseluruhan unit dalam satu kesatuan.*

**Nilai**

| | |
| --- | --- |
| Maksimum | 4 |
| Nilai | 3 |
| Review | Jawaban terlalu dangkal, tapi sudah tepat. |

#### Terangkan apa itu TDD.

**Jawaban**

*TDD adalah pengujian code yang berpacu dari test yang telah dibuat sebelum code. Pengujian dengan cara ini dapat mengurangi terjadinya kesalahan yang diakibatkan perubahan satu method yang digunakan di fungsi lain. Karena dengan cara ini kita bisa dapat merencanakan kesulitan apa saja yang akan terjadi sebelum menulis kodenya*

**Nilai**

| | |
| --- | --- |
| Maksimum | 4 |
| Nilai | 2 |
| Review | Jawaban terlalu dangkal, dan kurang lengkap. |